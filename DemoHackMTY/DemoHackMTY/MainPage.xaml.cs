﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Media;
using Microsoft.ProjectOxford.Vision;
using Microsoft.ProjectOxford.Vision.Contract;

namespace DemoHackMTY
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            btnTomarFoto.Clicked += BtnTomarFoto_Clicked;
        }

        private async void BtnTomarFoto_Clicked(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();
            if (CrossMedia.Current.IsCameraAvailable || CrossMedia.Current.IsTakePhotoSupported)
            {
                var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions()
                {
                    Name = "foto1.jpg",
                    Directory = "demo1",
                    SaveToAlbum = true
                });

                if (file != null)
                {
                    OcrResults text;
                    var client = new VisionServiceClient("9525ed1f5c64475e80014d5369d97acd");
                    using (var photoStream = file.GetStream())
                    {
                        text = await client.RecognizeTextAsync(photoStream, languageCode: "es");
                        StringBuilder sb = new StringBuilder();
                        foreach (var region in text.Regions)
                        {
                            foreach (var line in region.Lines)
                            {
                                foreach (var word in line.Words)
                                {
                                    sb.AppendLine(word.Text);
                                }
                            }
                        }
                        edtResultado.Text = sb.ToString();
                    }
                }
            }
        }
    }
}
